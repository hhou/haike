//
//  AppDelegate.m
//  HaikeFan
//
//  Created by Henry Hou on 3/16/15.
//  Copyright (c) 2015 Airdataservices. All rights reserved.
//

#import "AppDelegate.h"
#import <TheSidebarController/TheSidebarController.h>
#import "HomeViewController.h"
#import "MenuViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    HomeViewController *homeViewController = (HomeViewController*)[mainStoryboard
                                                       instantiateViewControllerWithIdentifier: @"HomeVC"];
    
    UINavigationController *contentNavigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    contentNavigationController.navigationBar.barTintColor = [UIColor colorWithRed:70/255.0 green:170/255.0 blue:221/255.0 alpha:1.0];
    contentNavigationController.navigationBar.translucent = NO;
    [contentNavigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    
    contentNavigationController.view.backgroundColor = [UIColor whiteColor];
    contentNavigationController.view.layer.shadowColor = [UIColor blackColor].CGColor;
    contentNavigationController.view.layer.shadowOffset = (CGSize){0.0, 0.0};
    contentNavigationController.view.layer.shadowOpacity = 0.6;
    contentNavigationController.view.layer.shadowRadius = 20.0;
    
    MenuViewController *menuSidebarViewController = (MenuViewController*)[mainStoryboard
                                                                   instantiateViewControllerWithIdentifier: @"MenuVC"];

    
    TheSidebarController *sidebarController = [[TheSidebarController alloc] initWithContentViewController:contentNavigationController
                                                                                leftSidebarViewController:menuSidebarViewController
                                                                               rightSidebarViewController:nil];
    
    sidebarController.delegate = self;
    sidebarController.view.backgroundColor = [UIColor blackColor];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = sidebarController;
    [self.window makeKeyAndVisible];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
